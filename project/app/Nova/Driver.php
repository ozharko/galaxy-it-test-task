<?php

namespace App\Nova;

use App\Rules\BelowAge;
use Laravel\Nova\Fields\Currency;
use Laravel\Nova\Fields\Email;
use Vyuldashev\NovaMoneyField\Money;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Illuminate\Support\Str;
use Ebess\AdvancedNovaMediaLibrary\Fields\Images;

class Driver extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var class-string<\App\Models\Driver>
     */
    public static $model = \App\Models\Driver::class;

    public static function label()
    {
        return 'Водії';
    }

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public function title()
    {
        return $this->name . ' ' . $this->surname;
    }

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'name',
        'surname',
        'birthdate',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function fields(NovaRequest $request)
    {
        return [
            ID::make()->sortable(),
            Text::make('Імʼя', 'name')->displayUsing(function ($value) {
                return Str::title($value);
            })->resolveUsing(function ($value) {
                return Str::title($value);
            })->rules('required')->required(),
            Text::make('Прізвизе', 'surname')->displayUsing(function ($value) {
                return Str::title($value);
            })->resolveUsing(function ($value) {
                return Str::title($value);
            })->rules('required')->required(),
            Date::make('Дата народження', 'birthdate')
                        ->rules('required', new BelowAge(65)),
            Images::make('Images', 'images'),
            Currency::make('Заробітня плата', 'salary')->displayUsing(function($amount){
                return number_format((float) $amount / 100, 2, ',', ' ');
            })->resolveUsing(function ($value) {
                $value = str_replace(['$', ',', '€'], '', $value);
                return $value ? (float) $value / 100 : null;
            })->rules('required')->required(),
//            Money::make('Заробітня плата', 'UAH', 'salary')->resolveUsing(function ($value) {
//                $value = str_replace(['$', ',', '€'], '', $value);
//                return (float) $value / 100;
//            })->required(),
            Email::make('E-mail', 'email')->rules('required')->required()
                ->creationRules('unique:users,name')
                ->updateRules('unique:users,name,{{resourceId}}'),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function cards(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function filters(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function lenses(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function actions(NovaRequest $request)
    {
        return [];
    }
}
