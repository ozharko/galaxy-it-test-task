<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class OldDriverEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $license;
    /**
     * Create a new message instance.
     */
    public function __construct($name, $license)
    {
        $this->name = $name;
        $this->license = $license;
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            subject: 'Увага. Водій вийшов на пенсію',
        );
    }

    /**
     * Get the message content definition.
     */
    public function content(): Content
    {
        return new Content(
            view: 'mail.old-driver',
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    public function attachments(): array
    {
        return [];
    }

    public function build()
    {
        return $this->to(env('ADMIN_EMAIL'))->view('mail.old-driver', [
            'name' => $this->name,
            'license' => $this->license
        ]);
    }
}
