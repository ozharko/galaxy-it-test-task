<?php

namespace App\Http\Controllers;

use App\Models\Bus;
use App\Models\Driver;
use Illuminate\Http\Request;

class DriverController extends Controller
{
    public function delete(Request $request, $driver_id)
    {
        $driver = Driver::get_driver($driver_id);

        if (!$driver) {
            // Обробка, якщо запис не знайдено
            return response()->json(['error' => 'Водія не знайдено'], 404);
        }

        if ($driver->deleted_at) {
            // Обробка, якщо запис не знайдено
            return response()->json(['message' => 'Користувач вже був видалений']);
        }

        Driver::delete_driver($driver_id);
        Bus::clear_from_driver($driver_id);

        return response()->json(['message' => 'Користувач був успішно видалений']);
    }
}
