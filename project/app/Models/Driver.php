<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Illuminate\Database\Eloquent\SoftDeletes;

class Driver extends Model implements HasMedia
{
    use SoftDeletes;
    use InteractsWithMedia;
    use HasFactory;

    protected $casts = [
        'birthdate' => 'datetime:d.m.Y'
    ];

    protected $fillable = [
        'name',
        'surname',
        'salary',
    ];

//    protected $dates = ['deleted_at'];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = strtolower($value);
    }

    public function setSurnameAttribute($value)
    {
        $this->attributes['surname'] = strtolower($value);
    }

    public function setSalaryAttribute($value)
    {
        $this->attributes['salary'] = $value * 100;
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(130)
            ->height(130);
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('main')->singleFile();
        $this->addMediaCollection('my_multi_collection');
    }

    /*
     * Check driver who was deleted 24 hours before
     * */
    public static function get_24_hours_deleted_records()
    {
        return Driver::select('name', 'email')->withTrashed()->where('deleted_at', '<=', now()->subDay())->get();
    }

    public static function get_driver($driver_id)
    {
        return Driver::withTrashed()->find($driver_id);
    }

    public static function delete_driver($driver_id)
    {
        Driver::find($driver_id)->delete();
    }

    public static function old_to_drive()
    {
        $max_age = 65;
        return Driver::select(
            'drivers.id',
            'drivers.name',
            'drivers.email',
            'buses.license'
        )->whereDate('birthdate', '<=', now()->subYears($max_age))
            ->leftJoin('buses', 'buses.driver_id', '=', 'drivers.id')
            ->get();
    }
}
