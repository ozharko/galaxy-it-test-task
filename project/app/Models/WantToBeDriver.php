<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WantToBeDriver extends Model
{
    use HasFactory;

    protected $casts = [
        'birth_date' => 'datetime:d.m.Y'
    ];
}
