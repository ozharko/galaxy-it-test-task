<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bus extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'license',
        'driver_id',
    ];

    protected $dates = ['deleted_at'];

    public function setLicenseAttribute($value)
    {
        $this->attributes['license'] = strtoupper($value);
    }

    public function bus_model()
    {
        return $this->belongsTo(BusModel::class, 'bus_model_id');
    }

    public function driver()
    {
        return $this->belongsTo(Driver::class, 'driver_id');
    }

    public static function clear_from_driver($driver_id)
    {
        Bus::where('driver_id', $driver_id)->update([
            'driver_id' => null,
        ]);
    }
}
