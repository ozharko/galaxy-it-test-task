<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Bus;

class BusPolicy
{
    /**
     * Create a new policy instance.
     */
    public function __construct()
    {
        //
    }

    public function view(User $user)
    {
        return true;
    }

    public function create(User $user)
    {
        return true;
    }

    public function update(User $user, Bus $model)
    {
        return true;
    }

    public function delete(User $user, Bus $model)
    {
        return true;
    }
}
