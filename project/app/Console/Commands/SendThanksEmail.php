<?php

namespace App\Console\Commands;

use App\Mail\ThanksEmail;
use App\Models\Driver;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendThanksEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:send-thanks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $drivers = Driver::get_24_hours_deleted_records();

        foreach ($drivers as $driver) {
            Mail::to($driver->email)->send(new ThanksEmail($driver->name));
        }
    }
}
