<?php

namespace App\Console\Commands;

use App\Models\Driver;
use Illuminate\Console\Command;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class SendSalaryReminder extends Command
{
    protected $signature = 'reminder:send-salary';

    protected $description = 'Send salary reminder email to all drivers';

    public function handle()
    {
        $nextMonth = Carbon::now()->addMonth()->format('F'); // Наступний місяць

        $drivers = Driver::all();

        foreach ($drivers as $driver) {
            $currentSalary = $driver->salary / 100; // Поточна зарплата
            $email = $driver->email;
            $name = $driver->name;

            $message = "Ваша поточна зарплата становить $currentSalary грн, не забудьте отримати її 1-го $nextMonth.";

            Mail::raw($message, function ($mail) use ($email, $name) {
                $mail->to($email, $name)
                    ->subject('Нагадування про зарплату');
            });
        }

        $this->info('Листи успішно надіслано водіям.');
    }
}
