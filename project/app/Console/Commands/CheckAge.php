<?php

namespace App\Console\Commands;

use App\Mail\OldDriverEmail;
use App\Models\Bus;
use App\Models\Driver;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Jobs\SendSmsJob;
use Illuminate\Support\Facades\Queue;

class CheckAge extends Command
{
    protected $signature = 'check:age';

    protected $description = 'Check age and perform necessary actions';

    public function handle()
    {
        $drivers = Driver::old_to_drive();

        foreach ($drivers as $driver) {
            Driver::delete_driver($driver->id);
            Bus::clear_from_driver($driver->id);
            $this->send_sms($driver);
            $this->send_email($driver);
        }

        $this->info('Age check completed successfully.');
    }

    public function send_sms($driver)
    {
        $wait_time = 5;
        $name = $driver->name;
        $license = $driver->license;
        $phoneNumber = env('ADMIN_PHONE');
        $message = "Водій $name сьогодні вийшов не пенсію, автобус, номерний знак $license залишився без водія";

        $job = new SendSmsJob($phoneNumber, $message);
        Queue::later(now()->addMinutes($wait_time), $job);
    }

    public function send_email($driver)
    {
        $wait_time = 15;
        $email = new OldDriverEmail($driver->name, $driver->license);
        $delay = now()->addMinutes($wait_time);

        Mail::later($delay, $email);
    }
}
