<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class SmsService
{
    public function sendSms($phoneNumber, $message)
    {
        Http::get('https://alphasms.ua/api/http.php', [
            'version' => 'http',
            'key' => 'c2a4239074e16fe23a27b53d1f5bd70434d8c871',
            'from' => 'technohelp',
            'to' => $phoneNumber,
            'message' => $message,
        ]);
    }
}
