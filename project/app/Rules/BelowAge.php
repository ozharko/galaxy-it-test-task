<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Carbon\Carbon;

class BelowAge implements Rule
{
    protected $ageLimit;

    /**
     * Create a new rule instance.
     *
     * @param  int  $ageLimit
     * @return void
     */
    public function __construct($ageLimit)
    {
        $this->ageLimit = $ageLimit;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $dob = Carbon::parse($value);
        $age = $dob->age;

        return $age < $this->ageLimit;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "Водій має бути молодше {$this->ageLimit} років.";
    }
}
